<?php

namespace Sunnydevbox\CebuUnitedRebuilders\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PermissionValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' 		    => 'required|min:3',
            'guard_name' 	=> 'required|min:3',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'      => 'required|min:3',
        ]
   ];

}