<?php
namespace Sunnydevbox\CebuUnitedRebuilders\Factories;

class UserAuthenticationFactory
{
    private static $controller = null;
    
    public static function authenticate($controller, $credentials)
    {
        self::$controller = $controller;
        
        if (!filter_var($credentials['email'], FILTER_VALIDATE_EMAIL)) {
            return self::employeeAuthentication($credentials);
        } else {
            return self::defaultAuthentication($credentials);
        }
    }

    public static function employeeAuthentication($credentials)
    {
        
        \Config::set('auth.providers.users.model', config('tw-pim.models.employee'));// \Sunnydevbox\TWPim\Models\Employee::class);
        \Config::set('jwt.user', config('tw-pim.models.employee'));
        // dd(config('tw-pim.models.employee'), config('auth.providers.users.model'), config('jwt.user'));

        $credentials['employee_id_number'] = $credentials['email'];                
        unset($credentials['email']);

        if (! $token = \JWTAuth::attempt($credentials)) {
            // return response()->json(['error' => 'invalid_credentials'], 401);
            return self::$controller->response->errorUnauthorized('invalid_credentials');
        }
        
        $user = \JWTAuth::authenticate($token);

        // IF **NOT** INACTIVE
        if (!$user->hasRole('employee')) {
            return self::$controller->response->errorUnauthorized('no_permission'); 
        }

        if ($user->employment_status_id != 1) {
            $user->token = \JWTAuth::getToken()->get();
            return self::$controller->response->item($user, \Sunnydevbox\TWPim\Transformers\EmployeeTransformer::class);
        } else {
            return self::$controller->response->errorUnauthorized('not_verified');
        }
    }


    public static function defaultAuthentication($credentials)
    {
        if (! $token = \JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'invalid_credentials'], 401);
        }
        
        $user = \JWTAuth::authenticate($token);

        if ($user->status == 'inactive' || !$user->is_verified) {
            return self::$controller->response->errorUnauthorized('no_permission'); 
        } 
           // return self::$controller->response->errorUnauthorized('not_verified');

        self::$controller->transformer->setMode('complete');
        $user->token = \JWTAuth::getToken()->get();
        return self::$controller->response->item($user, self::$controller->transformer);
    }
}