<?php
namespace Sunnydevbox\CebuUnitedRebuilders\Services;

use Sunnydevbox\TWPim\Services\EmployeeIDAbstract;

class EmployeeIDService extends EmployeeIDAbstract
{
    protected function format()
    {
        $name = explode(' ', $this->object->first_name);
        $name = strtoupper($name[0]);
        $id = \Carbon\Carbon::parse($this->object->birthdate)->format('mdY') .
            $name;

        return $this->checkDuplicate($id);
    }

    protected function fallbackFormat()
    {
        $id = request()->get('employee_id_number');
        // $id = $this->checkDuplicate($id);
        if ( $id && $id != $this->object->employee_id_number )
        {
            $id = $this->checkDuplicate($id);
            return $id;
        }

        return false;
    }



    public function checkDuplicate($id)
    {
        $suffix = $id . '_DUP-' . str_pad(rand(1, 99999), 7, 0) ;

        $e = config('tw-pim.models.employee');
        $Employee = new $e;
        $employee = $Employee->where('employee_id_number', $id)
                        ->where('id', '!=', $this->object->id)
                        ;

        // dd($employee->toSql(), $employee->getBindings());
        $employee = $employee->first();
        if ($employee) {
            return $suffix;
        }

        return $id;
    }


    protected function conditionsToGenerate()
    {
        return empty(request()->get('employee_id_number'))? true : false;
    }
}
