<?php
namespace Sunnydevbox\CebuUnitedRebuilders\Services;

use Sunnydevbox\TWCore\Services\SupplementDataService as SuppdataService;

class SupplementDataService extends SuppdataService
{
    public function d($d)
    {
        switch($d) {
            case 'roles':
                return $this->rpoRole->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'permissions':
                return $this->rpoPermission->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'civil-status':
                return $this->rpoCivilStatus->orderBy('name', 'asc')->all()->toArray();
            break;
            
            case 'company-departments':
                return $this->rpoCompanyDepartment->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'deductions':
                return $this->rpoDeduction->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'benefits':
                return $this->rpoBenefit->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'holidays':
                return $this->rpoHoliday->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'holiday-types':
                return $this->rpoHolidayType->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'job-positions':
                return $this->rpoJobPosition->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'tax-status':
                return $this->rpoTaxStatus->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'titles':
                return $this->rpoTitle->all()->toArray();
            break;

            case 'employment-status':
                return $this->rpoEmploymentStatus->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'employment-types':
                return $this->rpoEmploymentType->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'shift-templates':
                return $this->rpoShiftTemplate->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'leave-types':
                return $this->rpoLeaveType->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'application-statuses':
                return $this->rpoLeaveApplication->makeModel()->getStatuses();
            break;

            case 'payroll-periods':
                return $this->rpoPayrollTemplate->makeModel()->getPayPeriods();
            break;

            case 'payroll-templates':
                return $this->rpoPayrollTemplate->makeModel()->select('id', 'name', 'month_days')->orderBy('name', 'asc')->get();
            break;

            case 'employee-templates':
                return $this->rpoEmployeeTemplate->makeModel()->select('id', 'name')->orderBy('name', 'asc')->get();
            break;

            default:
                return [];
            break;
        }
    }

    public function __construct(
        \Sunnydevbox\TWUser\Repositories\Role\RoleRepository $rpoRole,
        \Sunnydevbox\TWUser\Repositories\Permission\PermissionRepository $rpoPermission,

        \Sunnydevbox\TWPim\Repositories\CivilStatus\CivilStatusRepository $rpoCivilStatus,
        \Sunnydevbox\TWPim\Repositories\CompanyDepartment\CompanyDepartmentRepository $rpoCompanyDepartment,
        \Sunnydevbox\TWPim\Repositories\Deduction\DeductionRepository $rpoDeduction,
        \Sunnydevbox\TWPim\Repositories\Benefit\BenefitRepository $rpoBenefit,
        \Sunnydevbox\TWPim\Repositories\JobPosition\JobPositionRepository $rpoJobPosition,
        \Sunnydevbox\TWPim\Repositories\TaxStatus\TaxStatusRepository $rpoTaxStatus,
        \Sunnydevbox\TWPim\Repositories\Title\TitleRepository $rpoTitle,
        \Sunnydevbox\TWPim\Repositories\Employee\EmploymentStatusRepository $rpoEmploymentStatus,
        \Sunnydevbox\TWPim\Repositories\Employee\EmploymentTypeRepository $rpoEmploymentType,
        \Sunnydevbox\TWPim\Repositories\Holiday\HolidayRepository $rpoHoliday,
        \Sunnydevbox\TWPim\Repositories\Holiday\HolidayTypeRepository $rpoHolidayType,
        \Sunnydevbox\TWPim\Repositories\ShiftTemplate\ShiftTemplateRepository $rpoShiftTemplate,
        \Sunnydevbox\TWPim\Repositories\LeaveApplication\LeaveTypeRepository $rpoLeaveType,
        \Sunnydevbox\TWPim\Repositories\LeaveApplication\LeaveApplicationRepository $rpoLeaveApplication,
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollRepository $rpoPayroll,
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollTemplateRepository $rpoPayrollTemplate,
        \Sunnydevbox\TWPim\Repositories\Employee\EmployeeTemplateRepository $rpoEmployeeTemplate
    ) {
        $this->rpoRole              = $rpoRole;
        $this->rpoPermission        = $rpoPermission;

        $this->rpoCivilStatus       = $rpoCivilStatus;
        $this->rpoCompanyDepartment = $rpoCompanyDepartment;
        $this->rpoDeduction         = $rpoDeduction;
        $this->rpoBenefit           = $rpoBenefit;
        $this->rpoJobPosition       = $rpoJobPosition;
        $this->rpoTaxStatus         = $rpoTaxStatus;
        $this->rpoTitle             = $rpoTitle;
        $this->rpoEmploymentStatus  = $rpoEmploymentStatus;
        $this->rpoEmploymentType    = $rpoEmploymentType;
        $this->rpoHoliday           = $rpoHoliday;
        $this->rpoHolidayType       = $rpoHolidayType;
        $this->rpoShiftTemplate     = $rpoShiftTemplate;
        $this->rpoLeaveType         = $rpoLeaveType;
        $this->rpoLeaveApplication  = $rpoLeaveApplication;
        $this->rpoPayroll           = $rpoPayroll;
        $this->rpoPayrollTemplate   = $rpoPayrollTemplate;
        $this->rpoEmployeeTemplate  = $rpoEmployeeTemplate;
    }
}