<?php

namespace Sunnydevbox\CebuUnitedRebuilders\Transformers;

use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;
use League\Fractal\TransformerAbstract;
use Sunnydevbox\CebuUnitedRebuilders\Models\Role;

class RoleTransformer extends TransformerAbstract
{
    public function transform(Role $obj)
    {
        return [
            'id'    		=> (int) $obj->id,
            'name' 			=> $obj->name,
        ];
    }
}