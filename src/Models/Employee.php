<?php
namespace Sunnydevbox\CebuUnitedRebuilders\Models;
use Carbon\Carbon;

class Employee extends \Sunnydevbox\TWPim\Models\Employee
{
    use \Sunnydevbox\TWCore\Repositories\TWMetaTrait;

    protected $hidden =[
        'password',
    ];

    /** GETTERS/SETTER **/
    public function getBioIdAttribute()
    {
        return $this->getMeta('bio_id', null);
    }

    public function getSSSNumberAttribute()
    {
        return $this->getMeta('sss_number', null);
    }

    public function getTinNumberAttribute()
    {
        return $this->getMeta('tin_number', null);
    }

    /** SCOPES **/
    public function scopeWhereBioId($query, $bio_id = null)
    {   
        $query->whereHas('meta', function($query) use ($bio_id){
            $query->where('key', 'bio_id')
                ->where('value', $bio_id);
        });
    }


    /** HELPERS **/
    public function SSS_10th()
    {
        return substr(str_replace('-', '', $this->sss_number), 9, 1);
    }
    

    public function serviceDurationInMonths()
    {
        $dateHired = Carbon::parse($this->attributes['date_hired']);
        return $dateHired->diffInMonths(Carbon::now());
    }

    public function serviceDurationInYears()
    {
        $dateHired = Carbon::parse($this->attributes['date_hired']);
        return $dateHired->diffInYears(Carbon::now());
    }

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        
        $this->meta[] = 'bio_id';
        $this->meta[] = 'vacation_leave_credits';
        $this->meta[] = 'sick_leave_credits';
        $this->meta[] = 'emergency_leave_credits';
        $this->meta[] = 'birthday_leave_credits';
        $this->meta[] = 'maternity_leave_credits';
        $this->meta[] = 'sss_number';
        $this->meta[] = 'tin_number';
        
        $this->appends[] = 'bio_id';
        $this->appends[] = 'sss_number';
        $this->appends[] = 'tin_number';
    }
}