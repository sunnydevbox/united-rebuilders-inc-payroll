<?php
namespace Sunnydevbox\CebuUnitedRebuilders\Models;

use \Sunnydevbox\TWCore\Repositories\TWMetaTrait;
use \Sunnydevbox\TWUser\Models\TWUserMetaTrait;

class Timelog extends \Sunnydevbox\TWPim\Models\Timelog
{
    use TWUserMetaTrait;

    protected $meta = [
        'is_overtime_rendered',
    ];

    public function setIsOvertimeRenderedAttribute($value)
    {
        $this->saveMeta('is_overtime_rendered', ($value) ? true : false);
    }

    public function getIsOvertimeRenderedAttribute()
    {
        return $this->getMeta('is_overtime_rendered', null);
    }

    public function is_overtime_rendered()
    {
        return $this->getIsOvertimeRenderedAttribute();
    }

    public static function boot()
    {
        parent::boot();
        Timelog::observe(new \Sunnydevbox\TWPim\Observers\TimelogObserver);
    }
}