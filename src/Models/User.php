<?php
namespace Sunnydevbox\CebuUnitedRebuilders\Models;

use \Sunnydevbox\TWUser\Models\User as TWUserModel;
use \Sunnydevbox\TWCore\Repositories\TWMetaTrait;
use \Sunnydevbox\TWUser\Models\TWUserMetaTrait;

class User extends TWUserModel
{
    use TWUserMetaTrait;

    protected $appends = [
        'first_name', 
        'last_name',
    ];

    protected $hidden =[
        'password',
    ];

    protected $meta = [
        'first_name',
        'middle_name',
        'last_name',
        'phone',
        'address_1',
        'city',
        'state',
        'zipcode',
    ];

    public function setFirstNameAttribute($value)
    {
        $this->setMeta('first_name', $value);
    }

    public function getFirstNameAttribute()
    {
        return $this->getMeta('first_name');
    }

    public function getLastNameAttribute()
    {
        return $this->getMeta('last_name');
    }

}