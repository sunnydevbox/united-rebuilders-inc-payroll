<?php
namespace Sunnydevbox\CebuUnitedRebuilders\Models;

class PayrollLog extends \Sunnydevbox\TWPim\Models\PayrollLog
{
    use \Sunnydevbox\TWCore\Repositories\TWMetaTrait;

    public function getIncludeSssAttribute()
    {
        $result = $this->getMeta('include_sss', null);
        if (!$result || $result == 'false'  || $result == '0') {
            return false;
        }

        return true;
    }

    public static function boot()
    { 
        parent::boot();       
    }
    
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->meta[] = 'include_sss';
               
        $this->appends[] = 'include_sss';
    }
}