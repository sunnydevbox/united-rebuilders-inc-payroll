<?php
namespace Sunnydevbox\CebuUnitedRebuilders\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class SetupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cebuunitedrebuilders:run-setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CebuUnitedRebuilders - run setup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
        Artisan::call('cebuunitedrebuilders:publish-config');
        Artisan::call('twuser:run-setup');
        Artisan::call('migrate');
        Artisan::call('cebuunitedrebuilders:migrate', [
            '--action' => 'run',
        ]);
    }

    public function fire()
    {
        echo 'fire';
    }
}