<?php
namespace Sunnydevbox\CebuUnitedRebuilders;

use Sunnydevbox\TWCore\BaseServiceProvider;

class CebuUnitedRebuildersServiceProvider extends BaseServiceProvider
{
    public function mergeConfig()
    {
        return [
           realpath(__DIR__ . '/../config/config.php') => 'cebuunitedrebuilders'
        ];
    }

    public function loadRoutes()
    {
        return [
            realpath(__DIR__.'/../routes/api.php')
        ];
    }

    public function loadViews()
    {
        return [
            __DIR__.'/../resources/views' => 'cebuunitedrebuilders',
        ];
    }
    
    public function registerProviders()
    {
        // EVENTS
        if (class_exists('\Sunnydevbox\CebuUnitedRebuilders\EventServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\CebuUnitedRebuilders\EventServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\CebuUnitedRebuilders\EventServiceProvider::class);    
        }

        if (class_exists('\Sunnydevbox\TWUser\TWUserServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWUser\TWUserServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWUser\TWUserServiceProvider::class);    
        }

        if (class_exists('\Sunnydevbox\TWPim\TWPimServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWPim\TWPimServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWPim\TWPimServiceProvider::class);    
        }
        

        // if (class_exists('\Sunnydevbox\TWPayroll\TWPayrollServiceProvider')
        //     && !$this->app->resolved('\Sunnydevbox\TWPayroll\TWPayrollServiceProvider')
        // ) {
        //     $this->app->register(\Sunnydevbox\TWPayroll\TWPayrollServiceProvider::class);    
        // }

    }

    public function registerCommands()
    {
        if ($this->app->runningInConsole()) {

            $this->commands([
                \Sunnydevbox\CebuUnitedRebuilders\Console\Commands\PublishConfigCommand::class,
                \Sunnydevbox\CebuUnitedRebuilders\Console\Commands\MigrateCommand::class,
                \Sunnydevbox\CebuUnitedRebuilders\Console\Commands\SetupCommand::class,
            ]);
        }
    }
}