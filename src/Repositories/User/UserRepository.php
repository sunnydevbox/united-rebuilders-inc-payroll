<?php
namespace Sunnydevbox\CebuUnitedRebuilders\Repositories\User;

use Sunnydevbox\TWUser\Repositories\User\UserRepository as ExtendUserRepository;

class UserRepository extends ExtendUserRepository 
{
    protected $fieldSearchable = [
        'email'
    ];

    public function model()
    {
        return \Sunnydevbox\CebuUnitedRebuilders\Models\User::class;
    }
}