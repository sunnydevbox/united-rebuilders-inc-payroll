<?php

namespace Sunnydevbox\CebuUnitedRebuilders\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\CebuUnitedRebuilders\Models\User;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // GENERATE THE NEW TOKEN
        $token = app('auth.password.broker')->createToken($this->user);

        return $this->view('tw-user::mail.reset-password')
                    ->with([
                        'user' => $this->user->getMeta('first_name'),
                        'reset_url' => 

                        config('app.url')
                        . 'users/reset-password/' 
                        . urlencode($this->user->email) 
                        . '/' 
                        . $token,
                    ])
                    ->subject('RecoverHub :: Your request to reset password')
                    //->from()
                    ->to($this->user->email, $this->user->first_name);
    }
}
