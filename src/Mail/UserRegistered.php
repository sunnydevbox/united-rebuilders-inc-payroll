<?php

namespace Sunnydevbox\CebuUnitedRebuilders\Mail;
//namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\CebuUnitedRebuilders\Models\User;

class UserRegistered extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('tw-user::mail.verification')
                    ->with([
                        'user' => $this->user->getMeta('first_name'),
                        'verification_url' => 

                        config('app.url')  // CALL THIS FROM the config() instead
                        . 'users/verify/' 
                        . urlencode($this->user->email) 
                        . '/' 
                        . $this->user->verification_token,
                    ])
                    ->subject('RecoverHub :: Please verify your email')
                    //->from()
                    ->to($this->user->email, $this->user->first_name);
    }
}
