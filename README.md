# RecoveryHub API
CebuUnitedRebuilders API. Video conferencing for doctors and patients.


# Installation
1) Install and configure a fresh instance of Laravel 5.4.x
```sh
$ composer create-project laravel/laravel="5.4.*" folder-name
```
2) composer.json: Include this in the repositories section
```javascript
"minimum-stability": "dev",
"repositories": [
    ...
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-core.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-user.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/united-rebuilders-inc-payroll.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-pim.git"
        }
        ...
    ]
```

3) Composer: Require the CebuUnitedRebuilders package
```sh
$ composer require sunnydevbox/united-rebuilders-inc-payroll:dev-master
```

4) Update config/app.php. Add these under 'providers'
```ssh
Sunnydevbox\CebuUnitedRebuilders\CebuUnitedRebuildersServiceProvider::class,
```

5) Update
```sh
$ composer dumpautoload
```

# Installation

1) Remove all migration files from database/migrations

2) Configure database settings in .env file
```ssh
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

3) Publish CebuUnitedRebuilders initial migration files
```sh
$ php artisan cebuunitedrebuilders:run-setup 
```

4) Manage CebuUnitedRebuilders migrations
```sh
$ php artisan cebuunitedrebuilders:migrate --action=[run/reset/refresh/rollback]
```


5) Configure .env
```javascript
API_NAME="Cebu United Rebuilders Inc. API"
API_STRICT=true
API_VERSION=v1
API_PREFIX=api
API_DEFAULT_FORMAT=json
API_DEBUG=true
API_SUBTYPE=cebuunitedrebuilders

//////////////////////////////////
// OPTIONAL FOR development use //
// Note: these values could     //
//       change anytime without //
//       notice                 //
//////////////////////////////////
MAIL_DRIVER=mailgun
MAIL_HOST=smtp.mailgun.org
MAIL_PORT=2525
MAIL_USERNAME=postmaster@api.recoveryhub.world
MAIL_PASSWORD=c19ea7a60d9965b334f422e92f57be2f-833f99c3-7e109255
MAIL_ENCRYPTION=null

MAIL_FROM_ADDRESS=team.recoveryhub.ph
MAILGUN_DOMAIN=api.recoveryhub.world
MAILGUN_SECRET=key-685f161bc452fb1591bc805dd31c9faf

```

7) Empty all route files in root directory


9) Update config/api.php
```javascript
'middleware' => [
        \Barryvdh\Cors\HandleCors::class,
    ],
'auth' => [
        'jwt' => 'Dingo\Api\Auth\Provider\JWT',
    ],
```

10) Update config/jwt.php
```javascript
'user' => \Sunnydevbox\CebuUnitedRebuilers\Models\User::class,
```
11) Update config/auth.php
```javascript
'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => \Sunnydevbox\CebuUnitedRebuilers\Models\User::class,
        ],
],
```

8) Clear all cache
```sh
$ php artisan twcore:optimize
```

# Server Configuration
Coming soon

# Queue Configuration
Coming soon





