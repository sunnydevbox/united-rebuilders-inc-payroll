# United Rebuilders Inc Payroll


## Requirements
1. PHP 7.1 or higher
2. Mysql 5.x

## Initial Configuration
1) Install and configure a fresh instance of Laravel 5.4.x

   `composer create-project laravel/laravel="5.4.*" project-name`

2) composer.json: Include this in the repositories section
```javascript
"repositories": [
    ...
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-core.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-user.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-pim.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-payroll.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/united-rebuilders-inc-payroll.git"
        }
        ...
    ]
```

```javascript
    ...
    "minimum-stability" : "dev"
    ...
```

3) Composer: Require the package
```sh
composer require sunnydevbox/united-rebuilders-inc-payroll:dev-master
```

4) Update config/app.php. Add these under 'providers'
```ssh
Barryvdh\Cors\ServiceProvider::class,
Sunnydevbox\CebuUnitedRebuilders\CebuUnitedRebuildersServiceProvider::class,
```
5) Routes

    Remove any closure routes in all routes file. This will cause the core optimizer to fail.

6) Configure .env file
    
    * Database credentials
    * Mail
    * API details

7) Update 
```ssh
> composer update
> php artisan twcore:optimize
```

## Getting Started
1. Setup core package
```ssh
php artisan twcore:publish-migrations
php artisan twcore:publish-config
php artisan twcore:optimize
```

2. Setup users package
```ssh
php artisan twuser:publish-migrations
php artisan twuser:publish-config
php artisan twcore:optimize
```

3. Update models in config/permission.php
```javascript
'models' => [
    ...

    'permission' => Sunnydevbox\TWUser\Models\Permission::class,
    'role' => Sunnydevbox\TWUser\Models\Role::class,

    ...
]
```
4. config/api.php
```javascript
'auth' => [
        'jwt' => 'Dingo\Api\Auth\Provider\JWT',
    ],
```

5. config/jwt.php
```javascript
'user' => '\Sunnydevbox\CebuUnitedRebuilders\Models\User',
```

6. Run migrations
```ssh
php artisan migrate
```

## Package Installation

1) Manage migrations
```sh
$ php artisan cebuunitedrebuilders:migrate --action[run/reset/refresh/rollback]
```
2) Clear all cache
```sh
$ php artisan twcore:optimize
```
3) Configure .env
```javascript
API_NAME="Cebu United Rebuilders Inc. API"
API_STRICT=true
API_VERSION=v1
API_PREFIX=api
API_DEFAULT_FORMAT=json
API_DEBUG=true
API_SUBTYPE=cebuunitedrebuilders
```
4, Configure config/auth.php
```php
'providers' => [
        'users' => [
            'driver' => 'eloquent',
            
            // Set this value to...
            'model' => Sunnydevbox\CebuUnitedRebuilders\Models\User::class,
        ],
]

```
5. Refresh cache
```sh
$ php artisan twcore:optimize
```

6. add cors middleware to kernel.php

7. configure tw-user.php

8. configure queue - install queue table and failed jobs


## Server Configuration
1. Nginx
2. PHP-FPM 7.1+
3. PHP Mcrypt
4. PHP Mysql
5. PHP Mbstring

## Queue Configuration
Coming soon