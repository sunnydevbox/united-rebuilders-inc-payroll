<?php


if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) 
{
	$api = app('Dingo\Api\Routing\Router');

	$api->version('v1', ['middleware' => []], function ($api) {

	});

	$api->version('v1', ['middleware' => ['api.auth']], function ($api) {

		$api->get('suppdata', '\Sunnydevbox\CebuUnitedRebuilders\Http\Controllers\API\V1\SuppDataController@index');

        // $api->resource('', '');
	});
}